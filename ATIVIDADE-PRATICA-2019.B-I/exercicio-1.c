/*
  Uma certa empresa fez uma pesquisa de mercado para saber se as pessoas
  gostaram ou não do seu último produto lançado. Para isto, coletou o sexo do entrevistado
  e a sua resposta (sim ou não). Sabendo que foram entrevistadas 150 pessoas, fazer um
  algoritmo que calcule e mostre ao final:
  • O número de pessoas que responderam sim;
  • O número de pessoas que responderam não;
  • A percentagem de pessoas do sexo feminino que responderam sim;
  • A percentagem de pessoas do sexo masculino que responderam não;
  Para a resposta SIM/NÃO. Utilize uma variável do tipo CHAR, que armazena S ou N,
  ou use uma variável do tipo INT que armazena 1 (para SIM) e 2 (para NÃO).
*/
#include <stdio.h>
#include <string.h>

int numPessoasSim = 0;
int numPessoasNao = 0;
int numMasculino = 0;
int numFeminino = 0;

float percentualFemeninoSim = 0;
float percentualMasculinoNao = 0;

const int quantidadePessoas = 150;

int main() {

  char gostou[quantidadePessoas][1];
  char genero[quantidadePessoas][1];
  char fim;

  for (int i = 0; i < quantidadePessoas; i++) {
    printf("Gostou do meu último produto? [S/n] \n");
    fflush(stdin);
    fgets(gostou[i], quantidadePessoas, stdin);

    printf("Qual o seu gênero? [M/F] \n");
    fflush(stdin);
    fgets(genero[i], quantidadePessoas, stdin);

    if (gostou[i][0] == 'S' || gostou[i][0] == 's') {
      if (genero[i][0] != 'M' || genero[i][0] != 'm') {
        numFeminino++;
      }
      numPessoasSim++;
      
    } else {
      if (genero[i][0] == 'M' || genero[i][0] == 'm') {
        numMasculino++;
      }
      numPessoasNao++;
    }

    printf("Você quer continuar? [S/n] \n");
    fflush(stdin);
    scanf("%c", &fim);
    if (fim != '\n' || fim == 'N' || fim == 'n') {
      break;
    }
    
  }

  int total = numPessoasSim + numPessoasNao;
  printf("Número de pessoas que responderam sim: %d \n", numPessoasSim);
  printf("Número de pessoas que responderam não: %d \n", numPessoasNao);
  
  if (numPessoasSim == 0) {
    numPessoasSim = 1;
  }

  if (numPessoasNao == 0) {
    numPessoasNao = 1;
  }

  percentualFemeninoSim = ((float)numFeminino / numPessoasSim) * 100;
  percentualMasculinoNao = ((float)numMasculino / numPessoasNao) * 100;
  
  printf("Percentagem de pessoas do sexo feminino que responderam sim: %.2f%% \n", percentualFemeninoSim);
  printf("Percentagem de pessoas do sexo masculino que responderam não: %.2f%% \n", percentualMasculinoNao);

  return 0;
}
