/*
  Dizemos que uma matriz quadrada inteira é um quadrado mágico se a soma dos
  elementos de cada linha, a soma dos elementos de cada coluna e a soma dos elementos
  das diagonais principal e secundária são todas iguais. Dada uma matriz quadrada
  dimensão MxM, verifique se ela é um quadrado mágico através de um algoritmo.
  Exemplo de matriz quadrado mágico:
  8 0 7
  4 5 6
  3 10 2
*/

#include <stdio.h>
#include <stdlib.h>

int main() {
  int ordemMatriz;

  printf("Qual a ordem da matriz quadrada? ");
  scanf("%d", &ordemMatriz);

  int matrizQuadrada[ordemMatriz][ordemMatriz];
  int somaLinhas[ordemMatriz];
  int somaColunas[ordemMatriz];
  int somaLinhaIguais = 0;
  int somaColunaIguais = 0;
  int somaDiagonalPrincipal = 0;
  int somaDiagonalSecundaria = 0;

  printf("Digite os valores da Matriz quadrada. \n\n");

  for (int l = 0; l < ordemMatriz; l++) {
    for (int c = 0; c < ordemMatriz; c++) {
      printf("Linha %d Coluna %d: ", (l+1), (c+1));
      scanf("%d", &matrizQuadrada[l][c]);
    }
  }

  // Somando as linhas
  for (int l = 0; l < ordemMatriz; l++) {
    somaLinhas[l] = 0;
    for (int c = 0; c < ordemMatriz; c++) {
      somaLinhas[l] += matrizQuadrada[l][c];
      if (l == c) {
        somaDiagonalPrincipal += matrizQuadrada[l][c];
      }
    }
  }
  
  for (int i = 0; i < (ordemMatriz-1); i++) {
    if (somaLinhas[i] == somaLinhas[i+1]) {
      somaLinhaIguais = somaLinhas[i];
    } else {
      printf("A matriz não é um quadrado mágico.");
      exit(0);
    }
  }

  // Somando as colunas
  for (int c = 0; c < ordemMatriz; c++) {
    somaColunas[c] = 0;
    for (int l = 0; l < ordemMatriz; l++) {
      somaColunas[c] += matrizQuadrada[l][c];
      if ((l+c) == (ordemMatriz-1)) {
        somaDiagonalSecundaria += matrizQuadrada[l][c];
      }
    }
  }

  for (int i = 0; i < (ordemMatriz-1); i++) {
    if (somaColunas[i] == somaColunas[i+1]) {
      somaColunaIguais = somaColunas[i];
    } else {
      printf("A matriz não é um quadrado mágico.");
      exit(0);
    }
  }

  printf("Soma das linhas: %d\n", somaLinhaIguais);
  printf("Soma das colunas: %d\n", somaColunaIguais);
  printf("Soma da diagonal principal: %d\n", somaDiagonalPrincipal);
  printf("Soma da diagonal secundária: %d\n", somaDiagonalSecundaria);

  if (somaLinhaIguais == somaColunaIguais
      && somaColunaIguais == somaDiagonalPrincipal
      && somaDiagonalPrincipal == somaDiagonalSecundaria) {
    printf("A MATRIZ É UM QUADRADO MÁGICO.");
  } else {
    printf("A matriz NÃO é um quadrado mágico.");
  }
  

  // lendo a matriz
  // for (int l = 0; l < ordemMatriz; l++) {
  //   for (int c = 0; c < ordemMatriz; c++) {
  //     printf("Linha %d Coluna %d: %d\n", (l+1), (c+1), matrizQuadrada[l][c]);
  //   }
    
  // }
  
  
  return 0;
}
