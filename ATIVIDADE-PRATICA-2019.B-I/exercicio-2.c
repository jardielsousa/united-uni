/*
  Para cada um dos consumidores de energia elétrica de uma cidade é informado o
  número da conta e o total de KW consumido no mês. Sabendo-se que o custo do KW é de
  R$ 1,75, fazer um algoritmo para:
  • Armazenar e listar o número da conta, o total de KW consumidos e o valor a
    pagar de cada consumir cadastrado;
  • Listar o número da conta, o total de KW consumidos e o valor a pagar do
    consumidor que mais gastou e o que menos gastou;
  • Mostrar a média de consumo da cidade;
  • Mostrar o número de consumidores que ultrapassaram o consumo de 170
    KW;
  Armazene as informações em estruturas de vetores e/ou matrizes. Na tela, deve
  existir um MENU que pergunta ao usuário se ele deseja cadastrar um novo consumidor ou
  listar alguma informação (maior, menor, média, etc.).
*/

#include <stdio.h>

const float VALOR_KW = 1.75;
int quantidade = 100;
int indiceConsumidor = 0;

int numContaMaior, numContaMenor;
float kwPorMesMaior=0, kwPorMesMenor;

int main() {

  int menu;
  int numConta[quantidade];
  float kwPorMes[quantidade];
  int inicio = 1;
  float totalConsumidoCidade = 0;

  do {
    printf("\n");
    printf("1.Cadastrar Consumidor\n");
    printf("2.Maior consumo\n");
    printf("3.Menor consumo\n");
    printf("4.Média consumo\n");
    printf("5.Exibir consumos\n");
    printf("0.SAIR\n");
    scanf("%d", &menu);

    switch (menu) {
    case 1:
      printf("Qual o número da conta? ");
      scanf("%d", &numConta[indiceConsumidor]);

      printf("Kw consumido no mês:");
      scanf("%f", &kwPorMes[indiceConsumidor]);

      if (inicio) {
        kwPorMesMenor = kwPorMes[indiceConsumidor];
        numContaMenor = numConta[indiceConsumidor];

        kwPorMesMaior = kwPorMes[indiceConsumidor];
        numContaMaior = numConta[indiceConsumidor];

        inicio = 0;
      } else {
        if (kwPorMes[indiceConsumidor] < kwPorMesMenor) {
          kwPorMesMenor = kwPorMes[indiceConsumidor];
          numContaMenor = numConta[indiceConsumidor];          
        }

        if (kwPorMes[indiceConsumidor] > kwPorMesMaior) {
          kwPorMesMaior = kwPorMes[indiceConsumidor];
          numContaMaior = numConta[indiceConsumidor];
        }
        
      }
      
      totalConsumidoCidade += VALOR_KW*kwPorMes[indiceConsumidor];

      indiceConsumidor++;
      
      break;

    case 2:
      printf("Conta de maior consumo: %d com valor %.2f = %.2f\n", numContaMaior, kwPorMesMaior, (VALOR_KW*kwPorMesMaior));
      break;

    case 3:
      printf("Conta de menor consumo: %d com valor %.2f = %.2f\n", numContaMenor, kwPorMesMenor, (VALOR_KW*kwPorMesMenor));
      break;
    
    case 4:
      printf("Média de consumo da cidade: %.2f\n", (totalConsumidoCidade/indiceConsumidor));
      break;
    case 5:
      for (int i = 0; i < indiceConsumidor; i++) {
        printf("A conta %d consumiu %.2f no mês. Valor total da conta: %.2f \n", numConta[i], kwPorMes[i], (VALOR_KW * kwPorMes[i]));
      }
      break;
      
    default:
      break;
    }

  } while (menu != 0);

  for (int i = 0; i < indiceConsumidor; i++) {
    printf("A conta %d consumiu %.2f no mês. Valor total da conta: %.2f \n", numConta[i], kwPorMes[i], (VALOR_KW * kwPorMes[i]));
  }

  printf("\n");
  printf("Conta de maior consumo: %d com valor %.2f = %.2f\n", numContaMaior, kwPorMesMaior, (VALOR_KW*kwPorMesMaior));
  printf("Conta de menor consumo: %d com valor %.2f = %.2f\n", numContaMenor, kwPorMesMenor, (VALOR_KW*kwPorMesMenor));
  printf("Média de consumo da cidade: %.2f\n", (totalConsumidoCidade/indiceConsumidor));
  printf("Consumidores que ultrapassaram o consumo de 170kw: \n");

  for (int i = 0; i < indiceConsumidor; i++) {
    if (kwPorMes[i] > 170) {
      printf("A conta %d consumiu %.2f no mês. \n", numConta[i], kwPorMes[i]);
    }
  }

  return 0;
}
